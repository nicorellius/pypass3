"""
PyPass3

Dice Roll Optional, Mostly-Random Word, Number, and Mixed Character
Password Generator and Secret Manager

Copyright (c) 2017 Nick Vincent-Maloney <nicorellius@gmail.com>
"""

import datetime

import mongoengine
from flask_login import UserMixin


class Secret(mongoengine.EmbeddedDocument):

    account_name = mongoengine.StringField(required=True)
    login_string = mongoengine.StringField()
    # password = mongoengine.StringField()
    password = mongoengine.BinaryField()
    url = mongoengine.URLField()
    notes = mongoengine.StringField()
    created = mongoengine.DateTimeField(required=True,
                                        default=datetime.datetime.now)


class User(mongoengine.Document, UserMixin):

    name = mongoengine.StringField()
    username = mongoengine.StringField(required=True)
    social_id = mongoengine.StringField(required=True)
    email = mongoengine.EmailField()
    created = mongoengine.DateTimeField(required=True,
                                        default=datetime.datetime.now)
    # avatar = mongoengine.ImageField()

    passwords = mongoengine.EmbeddedDocumentListField(Secret)

    meta = {
        'db_alias': 'core',
        'collection': 'users',
    }

    def __unicode__(self):
        return self.username

    @property
    def is_authenticated(self):
        return True

    @property
    def is_active(self):
        return True

    @property
    def is_anonymous(self):
        return False

    def get_id(self):
        return self.social_id
