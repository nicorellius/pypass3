"""
PyPass3

Dice Roll Optional, Mostly-Random Word, Number, and Mixed Character
Password Generator and Secret Manager

Copyright (c) 2017 Nick Vincent-Maloney <nicorellius@gmail.com>

"""

import os
import string
# import logging

# Secrets set here
# Flask application secret key
# with open('/etc/prv/pypass/flask_secret_key.txt') as secret_key:
#     FLASK_APP_SECRET_KEY = secret_key.read().strip()

# MongoDB database password
# with open('/etc/prv/pypass/mongodb_password.txt') as mongodb_password:
#     MONGO_DB_PASSWORD = mongodb_password.read().strip()

# RANDOM.ORG API Key
# with open('/etc/prv/pypass/rdo_api_key.txt') as api_key:
#     RDO_API_KEY = api_key.read().strip()

PROJECT_ROOT = os.path.dirname(os.path.abspath(__file__))
# PROJECT_ROOT = os.path.dirname(os.path.dirname(os.path.realpath(__file__)))
STATIC_PATH = os.path.join(PROJECT_ROOT, 'static')
TEMPLATE_PATH = os.path.join(PROJECT_ROOT, 'templates')

# Word lists from EFF:
#     https://www.eff.org/deeplinks/2016/07/new-wordlists-random-passphrases
#
# Local files
# WORDLIST_LONG = 'word_lists/wordlist_long.txt'
# WORDLIST_SHORT = 'word_lists/wordlist_short.txt'

# Remote files:
# https://www.eff.org/files/2016/07/18/eff_large_wordlist.txt
WORDLIST_LONG = 'http://bit.ly/2mtdxEk'
# https://www.eff.org/files/2016/09/08/eff_short_wordlist_2_0.txt
WORDLIST_SHORT = 'http://bit.ly/2ogvDGr'

# Note that this API key is not secure, and you should request your own!!!
# API_KEY = RDO_API_KEY

_SPECIAL = '!@#$%^&*()_+=-?~'
ALPHANUMERIC = '{ascii}{digits}'.format(
    ascii=string.ascii_letters, digits=string.digits)
CHARACTERS = '{ascii}{digits}{special}'.format(
    ascii=string.ascii_letters, digits=string.digits, special=_SPECIAL)

ROC_API_MAX_LENGTH = 20

# Previously the below configuration went in appconf dict
# appconf = dict()
DEBUG = True  # change to False in production
TESTING = True
# SECRET_KEY = FLASK_APP_SECRET_KEY,
# Starting point for credentials
USERNAME = 'guest'
PASSWORD = 'password'
# Debug Toolbar and MongoEngine configuration
DEBUG_TB_INTERCEPT_REDIRECTS = False
DEBUG_TB_PANELS = [
    'flask_debugtoolbar.panels.versions.VersionDebugPanel',
    'flask_debugtoolbar.panels.timer.TimerDebugPanel',
    'flask_debugtoolbar.panels.headers.HeaderDebugPanel',
    'flask_debugtoolbar.panels.request_vars.RequestVarsDebugPanel',
    'flask_debugtoolbar.panels.template.TemplateDebugPanel',
    'flask_debugtoolbar.panels.logger.LoggingPanel',
    'flask_debugtoolbar.panels.profiler.ProfilerDebugPanel',
    'flask_mongoengine.panels.MongoDebugPanel',
]
# Mongo configuration
MONGODB_DB = 'pypass'
MONGODB_ALIAS = 'core'
MONGODB_HOST = 'localhost'
MONGODB_PORT = 27972
MONGODB_USERNAME = 'pypass'
# MONGODB_PASSWORD=MONGO_DB_PASSWORD

